// hide descriptions

function taxonomy_vtn_show_hide_desc(what){
  if( document.getElementById('taxonomy_vtn_'+what)){
     var table = document.getElementById('taxonomy_vtn_'+what);
     var spany = table.getElementsByTagName('SPAN');

     for(i=0;i<spany.length;i++){
        if (spany[i].className == 'description'){
          if (spany[i].style.display == ''){
              spany[i].style.display = 'none';
          }
          else {
              spany[i].style.display = '';
          }
        }
     }
  }
}

