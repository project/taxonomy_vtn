<?php

/**
 * Implementation of taxonomy_vtn_show_vocabularies().
 */
function taxonomy_vtn_show_vocabularies() {
  
  $vocs = taxonomy_get_vocabularies();
  
  if ( (count($vocs) == 1) && (variable_get('taxonomy_vtn_vocabularies_goto_terms_if_one', 0) == 1) ) {
    drupal_goto('taxonomy_vtn/voc/'. key($vocs));
  }

  //Check if the page hasn't already been cached. If not, create and cache it.
  $cache_data = cache_get('taxonomy_vtn_show_vocs')->data;
  if ($cache_data && variable_get('taxonomy_vtn_caching', FALSE)) {
    $output = $cache_data;
    //print 'z cache';
    return $output;
  }
  else {

    if ($vocs) {
    
      $show_empty_vocs = variable_get('taxonomy_vtn_vocabularies_show_empty_vocs', 1);
      $show_count_terms = variable_get('taxonomy_vtn_vocabularies_show_count_terms', 1);
      $show_voc_desc = variable_get('taxonomy_vtn_vocabularies_show_voc_desc', 1);
      $count_group_vocabularies =  variable_get('taxonomy_vtn_vocabularies_count_group_vocabularies', 1);
      $count_column_vocs =  variable_get('taxonomy_vtn_vocabularies_count_column_vocs', 2);
      $arr = array();
  
      foreach ( $vocs as $vid => $voc_obj ) {
        $terms = taxonomy_get_tree($vid);
        $tcount = count($terms);

        if ($show_count_terms) {
          $count_terms = ' ('. $tcount .')';
        }
        
        if ($show_voc_desc) {
          $voc_desc = '<br /><span class="description">'. $voc_obj->description .'</span>';
        }
        
        if ($tcount || $show_empty_vocs) {
          $index = strtoupper(substr($voc_obj->name, 0, $count_group_vocabularies));
          $arr[$index][] = l($voc_obj->name, 'taxonomy_vtn/voc/'. $voc_obj->vid) . $count_terms . $voc_desc;
        }
      }
      
      $xlanguage = language_default();
      // change locale to sort
      //if ( setlocale(LC_ALL, $xlanguage->language.'_'.strtoupper($xlanguage->language).'.UTF8') ){
      if (variable_get('taxonomy_vtn_use_setlocale', 1) == 1 && variable_get('taxonomy_vtn_server_support_setlocale', 0) ) {
        setlocale(LC_ALL, $xlanguage->language .'_'. strtoupper($xlanguage->language) .'.UTF8');
        ksort($arr, SORT_LOCALE_STRING);
        // set locale back to default
        setlocale(LC_ALL, NULL); // without this count($arr) not work properly
      }
      else {
        ksort($arr); //--- this not support locales
      }
      
    
      $cnt = ceil(count($arr)/$count_column_vocs);
      $k = 0; $col = 0;
      foreach ($arr as $i => $t) {
        if ($k == 0 || ($k%$cnt) == 0 || $k == $cnt) {
          $cell = 'cell'. ++$col;
        }
        $$cell .= theme('item_list', $t, $i, 'ul');
        ++$k;
      }

      if ($k) {
        // prepare cells and they width
        ($col < $count_column_vocs ) ? $ile = $col : $ile = $count_column_vocs;
        for ($num=1; $num <= $ile; ++$num) {
          $xcell = 'cell'. $num;
          $xcells[] = array('data' => $$xcell, 'style' => 'width:'. (100/$ile) .'%');
        }
        $row[] = $xcells;


        if ($show_voc_desc) {
          $output .= '<a href="javascript:taxonomy_vtn_show_hide_desc(\'vocabularies\');">'. t('Show / Hide descriptions') .'</a>';
        }

        $output .= theme('table', array(), $row, array('class' => 'taxonomy_vtn_vocabularies', 'id' => 'taxonomy_vtn_vocabularies'));

        //Cache the fully rendered page in case of aggressive caching
        if (variable_get('taxonomy_vtn_caching', FALSE)) {
          cache_set('taxonomy_vtn_show_vocs', $output);
        }

        return $output;
      }
      else {
      
        $output = t('No data to display. Probably you have set option to not show empty vocabularies.');
      
        //Cache the fully rendered page in case of aggressive caching
        if (variable_get('taxonomy_vtn_caching', FALSE)) {
          cache_set('taxonomy_vtn_show_vocs', $output);
        }
        return $output;
      }
    }
    else {
      $output = t('No data to display. Probably you not have any vocabularies');
    
      //Cache the fully rendered page in case of aggressive caching
      if (variable_get('taxonomy_vtn_caching', FALSE)) {
        cache_set('taxonomy_vtn_show_vocs', $output);
      }
        
      return $output;
    }
  } // end cache
}

/**
 * Implementation of taxonomy_vtn_show_terms().
 */
function taxonomy_vtn_show_terms($arg = '') {

  if (is_numeric($arg)) {
    $own_vid = (int)$arg;
    if ($voc_obj = taxonomy_vocabulary_load($own_vid)) {
      drupal_set_title(t('Terms in %vocabulary', array('%vocabulary' => $voc_obj->name)));
      $output = '<div class="description">'. $voc_obj->description .'</div>';
    }
    else {
      drupal_set_title(t('Bad vocabulary id'));
      $output = '<p>'. t('No data to display. Probably you not have a vocabulary with vid = !voc_id', array('!voc_id' => $own_vid)) .'</p>';
      $output .= '<div>'. l('<< '. t('Back to vocabularies'), 'taxonomy_vtn') .'</div>';
      return $output;
    }
  }
  else {
    drupal_set_message(t('Please select a vocabulary first'));
    drupal_goto('taxonomy_vtn');
  }


  //Check if the page hasn't already been cached. If not, create and cache it.
  $cache_data = cache_get('taxonomy_vtn_show_terms_'. $own_vid)->data;
  if ($cache_data && variable_get('taxonomy_vtn_caching', FALSE)) {
    $output = $cache_data;
    //print 'z cache';
    return $output;
  }
  else {

    // vocabulary vid is ok, lets do it

    $show_empty_terms = variable_get('taxonomy_vtn_terms_show_empty_terms', 1);
    $show_count_nodes = variable_get('taxonomy_vtn_terms_show_count_nodes', 1);
    $show_term_desc = variable_get('taxonomy_vtn_terms_show_term_desc', 1);
    $show_synonyms = variable_get('taxonomy_vtn_terms_show_synonyms', 1);
    $count_group_terms =  variable_get('taxonomy_vtn_terms_count_group_terms', 1);
    $count_column_terms =  variable_get('taxonomy_vtn_terms_count_column_terms', 2);
    

    if ( $terms = taxonomy_get_tree($own_vid) ) {
      $arr = array();
      $arr_terms = array();
      $row = array();
      $count_nodes = '';
     
       /// prepare own full structure with terms and synonyms and link to them
      foreach ($terms as $term) {
          //$cnt_nodes[$term->tid] = taxonomy_term_count_nodes($term->tid);
          
          $tcount = taxonomy_term_count_nodes($term->tid);
          // real count or total count with relations terms
          // real ...//$tcount = db_result(db_query("SELECT COUNT(nid) FROM {term_node} WHERE tid = %d", $term->tid));
            
          if ($show_count_nodes) {
            $count_nodes = ' ('. $tcount .')';
          }
          
          $term_desc = '';
          if ($show_term_desc && $term->description) {
            $term_desc = '<br /><span class="description">'. $term->description .'</span>';
          }
          
          if ($tcount || $show_empty_terms) {
            $arr_terms[$term->name]['term'][] = l($term->name, 'taxonomy_vtn/term/'. $term->tid) . $count_nodes . $term_desc ;
            // if is not empty term or we show empty terms
            // and we will show synonyms then get for this
            if ($show_synonyms) {
              $synonyms = taxonomy_get_synonyms($term->tid);
              foreach ($synonyms as $synonym_name) {
                // $synonym_term = taxonomy_get_synonym_root($synonym_name);
                // all synonyms have a target - this term
                $arr_terms[$synonym_name]['synonym'][] = l($term->name, 'taxonomy_vtn/term/'. $term->tid);
              }
            }
          }
      } // end foreach terms as term prepare
      
      
      // Now we make index with links to proper terms for synonyms
      foreach ($arr_terms as $name => $types) {
        $index = drupal_strtoupper(drupal_substr($name, 0, $count_group_terms));
        foreach ($types as $type => $links) {
            if ($type == 'term') {
              foreach ($links as $link) { // maybe is more than one
                $arr[$index][] = $link;
              }
            }
            else { // is a synonym then look for terms
              $x_links = array();
              foreach ($links as $link) { // propably is more than one
                $x_links[] = $link;
              }
              //$arr[$index][] = $name .'<br /> <small> ( '. t('Look at') .' : '. implode(', ', $x_links) . ' )</small>';
              $arr[$index][] = $name .'<br /> <small>'. t('Synonyms') .': '. implode(', ', $x_links) .'</small>';
            }
        }
      } // end foreach arr_terms
     
     
      // sort array key (index like A B C .. etc.)
      $xlanguage = language_default();
      // change locale to sort
      //if ( setlocale(LC_ALL, $xlanguage->language.'_'.strtoupper($xlanguage->language).'.UTF8') ){
      if (variable_get('taxonomy_vtn_use_setlocale', 1) == 1 && variable_get('taxonomy_vtn_server_support_setlocale', 0) ) {
        setlocale(LC_ALL, $xlanguage->language .'_'. strtoupper($xlanguage->language) .'.UTF8');
        ksort($arr, SORT_LOCALE_STRING);
        // set locale back to default
        setlocale(LC_ALL, NULL); // without this count($arr) not work properly
      }
      else {
        ksort($arr); //--- this not support languages
      }

      $cnt = ceil(count($arr)/$count_column_terms);
      $k = 0; $col = 0;
      foreach ($arr as $i => $t) {
        if ($k == 0 || ($k%$cnt) == 0 || $k == $cnt) {
          $cell = 'cell'. ++$col;
        }
        $$cell .= theme('item_list', $t, $i, 'ul');
        ++$k;
      }

      if ($k) {
        // prepare cells and they width
        ($col < $count_column_terms ) ? $ile = $col : $ile = $count_column_terms;
        for ($num=1; $num <= $ile; ++$num) {
          $xcell = 'cell'. $num;
          $xcells[] = array('data' => $$xcell, 'style' => 'width:'. (100/$ile) .'%');
        }
        $row[] = $xcells;

        $term_js = '';
        if ($show_term_desc) {
          $term_js = ' &nbsp; | &nbsp; <a href="javascript:taxonomy_vtn_show_hide_desc(\'terms\');">'. t('Show / Hide descriptions') .'</a>';
        }
        $output .= '<div>'. l('<< '. t('Back to vocabularies'), 'taxonomy_vtn') . $term_js .'</div>';

        $output .= theme('table', array(), $row, array('class' => 'taxonomy_vtn_terms', 'id' => 'taxonomy_vtn_terms'));

        //Cache the fully rendered page in case of aggressive caching
        if (variable_get('taxonomy_vtn_caching', FALSE)) {
          cache_set('taxonomy_vtn_show_terms_'. $own_vid, $output);
        }

        return $output;
      }
      else {
        $output .= '<p>'. t('No data to display. Probably you have set option to not show empty terms.') .'</p>';
        $output .= '<div>'. l('<< '. t('Back to vocabularies'), 'taxonomy_vtn') .'</div>';

        //Cache the fully rendered page in case of aggressive caching
        if (variable_get('taxonomy_vtn_caching', FALSE)) {
          cache_set('taxonomy_vtn_show_terms_'. $own_vid, $output);
        }

        return $output;
      }
    }// end if terms
    else {
      $output .= '<p>'. t('No data to display. Probably this vocabulary is empty.') .'</p>';
      $output .= '<div>'. l('<< '. t('Back to vocabularies'), 'taxonomy_vtn') .'</div>';

      //Cache the fully rendered page in case of aggressive caching
      if (variable_get('taxonomy_vtn_caching', FALSE)) {
        cache_set('taxonomy_vtn_show_terms_'. $own_vid, $output);
      }

      return $output;
    }
  
  } // end else if cache
  
}


/**
 * Implementation of taxonomy_vtn_show_nodes().
 */
function taxonomy_vtn_show_nodes($arg = '') {

  if (is_numeric($arg)) {
    $own_tid = (int)$arg;
    if ($term_obj = taxonomy_get_term($own_tid)) {
      drupal_set_title(t('Nodes of term @term_name', array('@term_name' => $term_obj->name)));
    }
    else {
      drupal_set_title(t('Bad term id'));
      $output = '<p>'. t('No data to display. Probably you not have a term with tid = !term_id', array('!term_id' => $own_tid)) .'</p>';
      $output .= '<div>'. l('<< '. t('Back to vocabularies'), 'taxonomy_vtn') .'</div>';
      return $output;
    }
  }
  else {
    drupal_set_message(t('Please select a vocabulary first'));
    drupal_goto('taxonomy_vtn');
  }

  //Check if the page hasn't already been cached. If not, create and cache it.
  $cache_data = cache_get('taxonomy_vtn_show_nodes_'. $own_tid)->data;
  if ($cache_data && variable_get('taxonomy_vtn_caching', FALSE)) {
    $output = $cache_data;
    //print 'z cache';
    return $output;
  }
  else {

    // TODO: settings for depth
    require_once('taxonomy_vtn_overwrites.inc.php');
    $nodes = taxonomy_vtn_select_nodes(array($own_tid), $operator = 'or', $depth = 'all', $pager = FALSE, $order = 'n.sticky DESC', 0);

    $show_count_comments = variable_get('taxonomy_vtn_nodes_show_count_comments', 1);
    $count_column_nodes = variable_get('taxonomy_vtn_nodes_count_column_nodes', 2);
    $show_empty_count_comments = variable_get('taxonomy_vtn_nodes_show_empty_count_comments', 0);
    $show_nodes_index = variable_get('taxonomy_vtn_nodes_show_nodes_index', 0);

    $count_comments = '';

    while ($node = db_fetch_object($nodes)) {
      $node = node_load($node->nid);
      // TODO: cut special chars
      $index = strtoupper(substr($node->title, 0, 1));
      //print_r($node);
      if ($show_count_comments) {
        if ( $node->comment_count || $show_empty_count_comments) {
          $count_comments = ' ('. format_plural($node->comment_count, '1 comment', '@count comments') .')';
        }
      }
      $arr[$index][] = l($node->title, 'node/'. $node->nid) . $count_comments;
      // for <li> style .... $arr[$index][] = array('data' => l($node->title,'node/'.$node->nid) . $count_comments, 'style' => '');
    }
    
    if ( count($arr) > 0 ) {

      $xlanguage = language_default();
      // change locale to sort
      //if ( setlocale(LC_ALL, $xlanguage->language.'_'.strtoupper($xlanguage->language).'.UTF8') ){
      if (variable_get('taxonomy_vtn_use_setlocale', 1) == 1 && variable_get('taxonomy_vtn_server_support_setlocale', 0) ) {
        setlocale(LC_ALL, $xlanguage->language .'_'. strtoupper($xlanguage->language) .'.UTF8');
        ksort($arr, SORT_LOCALE_STRING);
        // set locale back to default
        setlocale(LC_ALL, NULL); // without this count($arr) not work properly
      }
      else {
        ksort($arr); //--- this not support locales
      }

      // if more columns always show index
      if ($count_column_nodes > 1) {
        $show_nodes_index = 1;
        $attr = array();
      }

      $cnt = ceil(count($arr)/$count_column_nodes);
      $k = 0; $col = 0;
      foreach ($arr as $i => $t) {
        if ($k == 0 || ($k%$cnt) == 0 || $k == $cnt) {
          $cell = 'cell'. ++$col;
        }
        if ($show_nodes_index == 0) {
          $i = NULL;
          $attr = array('class' => 'simple-list');
        }
        $$cell .= theme('item_list', $t, $i, 'ul', $attr);
        ++$k;
      }

      // prepare cells and they width
      ($col < $count_column_nodes ) ? $ile = $col : $ile = $count_column_nodes;
      for ($num=1; $num <= $ile; ++$num) {
        $xcell = 'cell'. $num;
        $xcells[] = array('data' => $$xcell, 'style' => 'width:'. (100/$ile) .'%');
      }
      $row[] = $xcells;

      $output .= '<div class="description">'. $term_obj->description .'</div>';
      $output .= '<div>'. l('<< '. t('Back to terms'), 'taxonomy_vtn/voc/'. $term_obj->vid) .'</div>';
      $output .= theme('table', array(), $row, array('class' => 'taxonomy_vtn_nodes', 'id' => 'taxonomy_vtn_nodes'));

      //Cache the fully rendered page in case of aggressive caching
      if (variable_get('taxonomy_vtn_caching', FALSE)) {
        cache_set('taxonomy_vtn_show_nodes_'. $own_tid, $output);
      }

      return $output;
    }
    else {
      $output = '<p>'. t('No data to display. Probably this term is empty.') .'</p>';
      $output .= '<div>'. l('<< '. t('Back to terms'), 'taxonomy_vtn/voc/'. $term_obj->vid) .'</div>';
      
      //Cache the fully rendered page in case of aggressive caching
      if (variable_get('taxonomy_vtn_caching', FALSE)) {
        cache_set('taxonomy_vtn_show_nodes_'. $own_tid, $output);
      }

      return $output;
    }
  } // end if cache
}

